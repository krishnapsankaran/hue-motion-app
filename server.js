const express = require('express');
const app = express();
import { getSensorStatus } from './hue/getSensorStatus'

app.get('/get-status', (req, res) => {
    getSensorStatus((response) => {
        res.send(response)
    })
});

app.get('/', (req, res) => {
    res.send('<h2>Welcome to my Hue Q estimator page</h2>');
});

app.listen(3000, () => console.log('Hue mon app listening on port 3000!'));